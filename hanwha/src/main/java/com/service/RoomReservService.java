package com.service;

import com.model.dao.RoomDao;
import com.model.dto.RoomRequestDTO;
import com.model.dto.RoomRequestOuter;

public class RoomReservService {

    public int insertRoomRequest(RoomRequestOuter roomRequestOuter){

        for(RoomRequestDTO rrd :roomRequestOuter.getDataObj().getList())
            return RoomDao.getInstance().insertRequest(rrd);

        return 1;
    }
}
