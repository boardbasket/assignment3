package com.model.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoomRequestDTO {

        @SerializedName("RSRV_NO")
        private Integer rsrvNo;
        @SerializedName("REQUEST_NO")
        private Integer requestNo;
        @SerializedName("RSRV_DATE")
        private String rsrvDate;
        @SerializedName("LOC_CD")
        private String locCd;
        @SerializedName("ROOM_TYPE_CD")
        private String roomTypeCd;
        @SerializedName("ARRV_DATE")
        private String arrvDate;
        @SerializedName("OVNT_CNT")
        private Integer ovntCnt;
        @SerializedName("CHKOT_EXPT_DATE")
        private String chkotExptDate;
        @SerializedName("RSRV_CUST_NM")
        private String rsrvCustNm;
        @SerializedName("RSRV_CUST_TEL_NO2")
        private String rsrvCustTelNo2;
        @SerializedName("RSRV_CUST_TEL_NO3")
        private String rsrvCustTelNo3;
        @SerializedName("RSRV_CUST_TEL_NO4")
        private String rsrvCustTelNo4;
        @SerializedName("INHS_CUST_NM")
        private String inhsCustNm;
        @SerializedName("INHS_CUST_TEL_NO2")
        private String inhsCustTelNo2;
        @SerializedName("INHS_CUST_TEL_NO3")
        private String inhsCustTelNo3;
        @SerializedName("INHS_CUST_TEL_NO4")
        private String inhsCustTelNo4;
        @SerializedName("CUST_IDNT_NO")
        private String custIdntNo;
        @SerializedName("CUST_NO")
        private String custNo;

        /*
        @SerializedName("MEMB_NO")
        private String membNo;
        @SerializedName("CONT_NO")
        private String contNo;
        @SerializedName("PAKG_NO")
        private String pakgNo;
        @SerializedName("CPON_NO")
        private String cponNo;
        @SerializedName("RSRV_LOC_DIV_CD")
        private String rsrvLocDivCd;
        @SerializedName("RSRV_ROOM_CNT")
        private String rsrvRoomCnt;*/




}
