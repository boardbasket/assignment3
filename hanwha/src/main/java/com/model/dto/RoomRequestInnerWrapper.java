package com.model.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class RoomRequestInnerWrapper {
    @SerializedName("ds_rsrvInfo")
    List<RoomRequestDTO> list;
}
