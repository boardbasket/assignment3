package com.model.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoomRequestOuter {
    @SerializedName("Data")
    private RoomRequestInnerWrapper dataObj;
}
