package com.controller;

import com.google.gson.Gson;
import com.model.dto.RoomRequestDTO;
import com.model.dto.RoomRequestOuter;
import com.service.RoomReservService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

@WebServlet(name="roomReserv", value="/room_reserv")
public class RoomReservController extends HttpServlet {
    private final Gson gson = new Gson();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws IOException {
        BufferedReader br = request.getReader();

        RoomRequestOuter roomRequestOuter = gson.fromJson(br, RoomRequestOuter.class);//요청 서버에서 넘겨받은 json 값 파싱 & 바인딩
        RoomReservService roomReservService = new RoomReservService();
        roomReservService.insertRoomRequest(roomRequestOuter); //요청 서버 데이터 insert 하기


        br.close();
        BufferedWriter bw = new BufferedWriter(resp.getWriter());
        bw.write(gson.toJson(roomRequestOuter));
        bw.close();
    }
}
