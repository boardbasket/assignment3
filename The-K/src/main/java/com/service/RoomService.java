package com.service;

import com.model.dao.RoomDao;
import com.model.dto.RoomRequestDTO;
import com.model.dto.RoomRequestOuter;

public class RoomService {

    public int updateRequest(RoomRequestOuter roomRequestOuter) {
        for (RoomRequestDTO rrd : roomRequestOuter.getDataObj().getList())
            if (rrd.getRsrvNo() == null)
                throw new RuntimeException("예약번호가 비어있습니다.");
            else
                return RoomDao.getInstance().updateRequest(rrd);

        return 1;
    }

    public int insertRequest(RoomRequestDTO roomRequestDTO) {
        try {
            return RoomDao.getInstance().insertRequest(roomRequestDTO);
        } catch (RuntimeException e) {
            return -1;
        }
    }

}
