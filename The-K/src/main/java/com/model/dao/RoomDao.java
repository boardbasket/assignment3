package com.model.dao;

import com.model.dto.RoomRequestDTO;
import org.apache.commons.dbcp2.BasicDataSource;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class RoomDao {


    private static final RoomDao INSTANCE;

    private static final BasicDataSource ds = new BasicDataSource();

    private static final Properties sqlProp = new Properties();


    public int updateRequest(RoomRequestDTO roomRequestDTO) {
        Connection con = null;
        PreparedStatement pstmt = null;
        int result;

        try {

            con = ds.getConnection();// connection 생성
            con.setAutoCommit(false);// rollback 하기 위해 autoCommit 기능을 비활성화
            pstmt = con.prepareStatement(sqlProp.getProperty("update_request"), Statement.RETURN_GENERATED_KEYS);//property에서 쿼리 가져오기
            pstmt.setInt(1, roomRequestDTO.getRsrvNo());
            pstmt.setInt(2, roomRequestDTO.getRequestNo());
            result = pstmt.executeUpdate();
            con.commit();
            return result;
        } catch (SQLException e) {
            try {
                e.printStackTrace();
                if (con != null)
                    con.rollback();                 //과정 중 오류가 발생했을 시에 rollback
                throw new RuntimeException("쿼리 실행중 오류");
            } catch (SQLException e1) {
                throw new RuntimeException("rollback 중 오류 발생");     //rollback 중 에러 발생시 runtimeException 발생
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();                     //preparedStatement 자원 반납
                if (con != null)
                    con.close();                       //connection 자원 반납
            } catch (SQLException e) {
                throw new RuntimeException("자원 반납 중 오류");
            }

        }
    }

    public int insertRequest(RoomRequestDTO roomRequestDTO) throws RuntimeException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {

            con = ds.getConnection();// connection 생성
            con.setAutoCommit(false);// rollback 하기 위해 autoCommit 기능을 비활성화

            pstmt = con.prepareStatement(sqlProp.getProperty("insert_request"), Statement.RETURN_GENERATED_KEYS);//property에서 쿼리 가져오기
            pstmt.setString(1, roomRequestDTO.getCustNo());
            pstmt.setString(2, roomRequestDTO.getMembNo());
            pstmt.setString(3, roomRequestDTO.getCustIdntNo());
            pstmt.setString(4, roomRequestDTO.getContNo());
            pstmt.setString(5, roomRequestDTO.getPakgNo());
            pstmt.setString(6, roomRequestDTO.getCponNo());
            pstmt.setString(7, roomRequestDTO.getLocCd());
            pstmt.setString(8, roomRequestDTO.getRoomTypeCd());
            pstmt.setString(9, roomRequestDTO.getRsrvLocDivCd());
            pstmt.setString(10, roomRequestDTO.getArrvDate());//~
            pstmt.setString(11, roomRequestDTO.getRsrvRoomCnt());
            pstmt.setInt(12, roomRequestDTO.getOvntCnt());
            pstmt.setString(13, roomRequestDTO.getInhsCustNm());
            pstmt.setString(14, roomRequestDTO.getInhsCustTelNo2());
            pstmt.setString(15, roomRequestDTO.getInhsCustTelNo3());
            pstmt.setString(16, roomRequestDTO.getInhsCustTelNo4());
            pstmt.setString(17, roomRequestDTO.getRsrvCustNm());
            pstmt.setString(18, roomRequestDTO.getRsrvCustTelNo2());
            pstmt.setString(19, roomRequestDTO.getRsrvCustTelNo3());
            pstmt.setString(20, roomRequestDTO.getRsrvCustTelNo4());
            pstmt.setString(21, roomRequestDTO.getRefreshYn());
            pstmt.setString(22, roomRequestDTO.getRsrvDate());

            int result;                        //리턴할 값
            pstmt.executeUpdate();                  //쿼리 실행
            rs = pstmt.getGeneratedKeys();          //auto_increment 값 가져오기
            rs.next();                              //resultSet의 커서를 첫 row 에 맞춤
            result = rs.getInt(1);      //auto_increment 값을 가져옴
            con.commit();                           //connection 커밋
            roomRequestDTO.setRequestNo(result); //auto_increment 값 넣기
            return result;
        } catch (SQLException e) {
            try {
                e.printStackTrace();
                if (con != null)
                    con.rollback();                 //과정 중 오류가 발생했을 시에 rollback
                throw new RuntimeException("쿼리 실행중 오류");
            } catch (SQLException e1) {
                throw new RuntimeException(e1);     //rollback 중 에러 발생시 runtimeException 발생
            }
        } finally {
            try {
                if (rs != null)
                    rs.close();                        //resultSet 자원 반납
                if (pstmt != null)
                    pstmt.close();                     //preparedStatement 자원 반납
                if (con != null)
                    con.close();                       //connection 자원 반납
            } catch (SQLException e) {
                throw new RuntimeException("자원 반납 중 오류");
            }

        }
    }


    public static RoomDao getInstance() {
        return INSTANCE;
    }

    static {
        try {
            INSTANCE = new RoomDao();
        } catch (IOException e) {
            throw new RuntimeException("싱글톤 객체 오류");
        }
    }

    private RoomDao() throws IOException {
        InputStream is = getClass().getResourceAsStream("/dataSource.properties");
        InputStream sqlStream = getClass().getResourceAsStream("/sql.properties");
        Properties prop = new Properties();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            prop.load(is);
            sqlProp.load(sqlStream);
            ds.setUrl(prop.getProperty("data_source.url"));
            ds.setUsername(prop.getProperty("data_source.username"));
            ds.setPassword(prop.getProperty("data_source.password"));
            ds.setMaxIdle(Integer.parseInt(prop.getProperty("data_source.max_connection")));
            ds.setMinIdle(Integer.parseInt(prop.getProperty("data_source.min_connection")));
            ds.setMaxOpenPreparedStatements(Integer.parseInt(prop.getProperty("data_source.max_prepared_statement")));
        } catch (Exception e) {
            throw new IOException("DataSource 초기화중 오류발생");
        }

    }
}
