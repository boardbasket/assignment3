package com.model.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoomRequestDTO {

    @SerializedName("REQUEST_NO")
    private Integer requestNo;
    @SerializedName("RSRV_NO")
    private Integer rsrvNo;
    @SerializedName("CUST_NO")
    private String custNo;
    @SerializedName("MEMB_NO")
    private String membNo;
    @SerializedName("CUST_IDNT_NO")
    private String custIdntNo;
    @SerializedName("CONT_NO")
    private String contNo;
    @SerializedName("PAKG_NO")
    private String pakgNo;
    @SerializedName("CPON_NO")
    private String cponNo;
    @SerializedName("LOC_CD")
    private String locCd;
    @SerializedName("ROOM_TYPE_CD")
    private String roomTypeCd;
    @SerializedName("RSRV_LOC_DIV_CD")
    private String rsrvLocDivCd;
    @SerializedName("ARRV_DATE")
    private String arrvDate;
    @SerializedName("RSRV_ROOM_CNT")
    private String rsrvRoomCnt;
    @SerializedName("OVNT_CNT")
    private Integer ovntCnt;
    @SerializedName("INHS_CUST_NM")
    private String inhsCustNm;
    @SerializedName("INHS_CUST_TEL_NO2")
    private String inhsCustTelNo2;
    @SerializedName("INHS_CUST_TEL_NO3")
    private String inhsCustTelNo3;
    @SerializedName("INHS_CUST_TEL_NO4")
    private String inhsCustTelNo4;
    @SerializedName("RSRV_CUST_NM")
    private String rsrvCustNm;
    @SerializedName("RSRV_CUST_TEL_NO2")
    private String rsrvCustTelNo2;
    @SerializedName("RSRV_CUST_TEL_NO3")
    private String rsrvCustTelNo3;
    @SerializedName("RSRV_CUST_TEL_NO4")
    private String rsrvCustTelNo4;
    @SerializedName("REFERSH_YN")
    private String refreshYn;
    @SerializedName("RSRV_DATE")
    private String rsrvDate;

    public RoomRequestDTO(String custNo, String membNo, String custIdntNo, String contNo, String pakgNo, String cponNo, String locCd, String roomTypeCd, String rsrvLocDivCd, String arrvDate, String rsrvRoomCnt, Integer ovntCnt, String inhsCustNm, String inhsCustTelNo2, String inhsCustTelNo3, String inhsCustTelNo4, String rsrvCustNm, String rsrvCustTelNo2, String rsrvCustTelNo3, String rsrvCustTelNo4, String refreshYn, String rsrvDate) {
        this.custNo = custNo;
        this.membNo = membNo;
        this.custIdntNo = custIdntNo;
        this.contNo = contNo;
        this.pakgNo = pakgNo;
        this.cponNo = cponNo;
        this.locCd = locCd;
        this.roomTypeCd = roomTypeCd;
        this.rsrvLocDivCd = rsrvLocDivCd;
        this.arrvDate = arrvDate;
        this.rsrvRoomCnt = rsrvRoomCnt;
        this.ovntCnt = ovntCnt;
        this.inhsCustNm = inhsCustNm;
        this.inhsCustTelNo2 = inhsCustTelNo2;
        this.inhsCustTelNo3 = inhsCustTelNo3;
        this.inhsCustTelNo4 = inhsCustTelNo4;
        this.rsrvCustNm = rsrvCustNm;
        this.rsrvCustTelNo2 = rsrvCustTelNo2;
        this.rsrvCustTelNo3 = rsrvCustTelNo3;
        this.rsrvCustTelNo4 = rsrvCustTelNo4;
        this.refreshYn = refreshYn;
        this.rsrvDate = rsrvDate;
    }
}
