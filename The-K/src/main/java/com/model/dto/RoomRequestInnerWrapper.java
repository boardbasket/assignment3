package com.model.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class RoomRequestInnerWrapper {
    @SerializedName("ds_rsrvInfo")
    List<RoomRequestDTO> list;

    public RoomRequestInnerWrapper() {
    }

    public RoomRequestInnerWrapper(RoomRequestDTO roomRequestDTO){
        List<RoomRequestDTO> temp = new ArrayList<>();
        temp.add(roomRequestDTO);
        this.list = temp;
    }
}
