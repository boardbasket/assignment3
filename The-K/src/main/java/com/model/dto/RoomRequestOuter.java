package com.model.dto;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class RoomRequestOuter {

    public RoomRequestOuter() {
    }

    public RoomRequestOuter(RoomRequestDTO roomRequestDTO) {

        RoomRequestInnerWrapper roomRequestInnerWrapper = new RoomRequestInnerWrapper();

        List<RoomRequestDTO> temp = new ArrayList<>();
        temp.add(roomRequestDTO);

        roomRequestInnerWrapper.setList(temp);

        this.dataObj = roomRequestInnerWrapper;

    }

    @SerializedName("Data")
    private RoomRequestInnerWrapper dataObj;

}
