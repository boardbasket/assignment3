package com.controller;

import com.google.gson.Gson;
import com.model.dto.RoomRequestDTO;
import com.model.dto.RoomRequestOuter;
import com.service.RoomService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

@WebServlet("/test_request")
public class RoomRequestController extends HttpServlet {

    private final Gson gson = new Gson();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        //HttpServletRequest 객체에서 form 의 parameter 를 전부 가져옴

        String custNo = req.getParameter("CUST_NO");
        String membNo = req.getParameter("MEMB_NO");
        String custIdntNo = req.getParameter("CUST_IDNT_NO");
        String contNo = req.getParameter("CONT_NO");
        String pakgNo = req.getParameter("PAKG_NO");
        String cponNo = req.getParameter("CPON_NO");
        String locCd = req.getParameter("LOC_CD");
        String roomTypeCd = req.getParameter("ROOM_TYPE_CD");
        String rsrvLocDivCd = req.getParameter("RSRV_LOC_DIV_CD");
        String arrvDate = req.getParameter("ARRV_DATE");
        String rsrvRoomCnt = req.getParameter("RSRV_ROOM_CNT");
        Integer ovntCnt = Integer.parseInt(req.getParameter("OVNT_CNT"));
        String inhsCustNm = req.getParameter("INHS_CUST_NM");
        String inhsCustTelNo2 = req.getParameter("INHS_CUST_TEL_NO2");
        String inhsCustTelNo3 = req.getParameter("INHS_CUST_TEL_NO3");
        String inhsCustTelNo4 = req.getParameter("INHS_CUST_TEL_NO4");
        String rsrvCustNm = req.getParameter("RSRV_CUST_NM");
        String rsrvCustTelNo2 = req.getParameter("RSRV_CUST_TEL_NO2");
        String rsrvCustTelNo3 = req.getParameter("RSRV_CUST_TEL_NO3");
        String rsrvCustTelNo4 = req.getParameter("RSRV_CUST_TEL_NO4");
        String refreshYn = req.getParameter("REFRESH_YN");
        String rsrvDate = req.getParameter("RSRV_DATE");


        //DTO 객체에 바인딩
        RoomRequestDTO roomRequestDTO = new RoomRequestDTO(custNo, membNo, custIdntNo, contNo, pakgNo, cponNo, locCd, roomTypeCd, rsrvLocDivCd, arrvDate, rsrvRoomCnt, ovntCnt, inhsCustNm, inhsCustTelNo2, inhsCustTelNo3, inhsCustTelNo4, rsrvCustNm, rsrvCustTelNo2, rsrvCustTelNo3, rsrvCustTelNo4, refreshYn, rsrvDate);

        RoomService roomService = new RoomService();    //service 객체 생성
        roomService.insertRequest(roomRequestDTO);    //예약 요청 데이터를 insert 할 메소드 실행

        RoomRequestOuter roomRequestOuter = new RoomRequestOuter(roomRequestDTO);     //JSON 형식으로 바꾸기 위해 객체에 집어 넣어줌

        HttpURLConnection conn = getHttpUrlConnection("http://localhost:18080/room_reserv", "POST"); //httpUrlConnection 생성
        OutputStream os = conn.getOutputStream();           //body에 값을 입력하기 위해 outputStream 객체 생성
        String json = gson.toJson(roomRequestOuter);        // json 형식으로 변경
        os.write(json.getBytes(StandardCharsets.UTF_8));    //output stream에 write
        os.flush();
        os.close();

        //응답
        InputStream is = conn.getInputStream();
        InputStreamReader isr = new InputStreamReader(is,StandardCharsets.UTF_8);
        RoomRequestOuter result = gson.fromJson(isr, RoomRequestOuter.class);
        roomService.updateRequest(result);

        conn.disconnect();
        req.setAttribute("result", result);
        req.getRequestDispatcher("/result.jsp").forward(req, resp);
    }

    private HttpURLConnection getHttpUrlConnection(String adderess, String method) throws IOException {
        URL url = new URL(adderess);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod(method);
        conn.setRequestProperty("Content-type", "application/json; utf-8");
        conn.setRequestProperty("Accept", "application/json");
        conn.setDoOutput(true);
        conn.setDoInput(true);

        return conn;
    }
}
