<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>과제 3</title>
</head>
<body>
<h1><%= "예약 정보 입력" %>
</h1>
<br/>
<form action="${pageContext.request.contextPath}/test_request" method="post">

    <div style="display:flex;flex-direction: column; align-items:flex-start;justify-content: flex-start;" >
        <input type="hidden" id="CUST_NO" name="CUST_NO" value="0000000002"/>
        <input type="hidden" name="MEMB_NO" value="195176">
        <input type="hidden" name="CUST_IDNT_NO" value="195176">
        <input type="hidden" name="CONT_NO" value="" maxlength="10">
        <label for="PAKG_NO">패키지 번호</label><input type="text" id="PAKG_NO" name="PAKG_NO" value="" maxlength="10">
        <label for="CPON_NO">쿠폰 번호</label><input type="text" id="CPON_NO" name="CPON_NO" value="" maxlength="10">
        <label for="LOC_CD">영업장 코드</label><input type="text" id="LOC_CD" name="LOC_CD" value="0201" maxlength="4">
        <label for="ROOM_TYPE_CD">객실 타입 코드</label><input type="text" id="ROOM_TYPE_CD" name="ROOM_TYPE_CD" value="FAM"
                                                     maxlength="3">
        <label for="RSRV_LOC_DIV_CD">S/C</label><input type="text" id="RSRV_LOC_DIV_CD" name="RSRV_LOC_DIV_CD"
                                                        value="S" maxlength="1">
        <label for="ARRV_DATE">도착 일자</label><input type="text" id="ARRV_DATE" name="ARRV_DATE" value="20230528"
                                                  maxlength="8" minlength="8">
        <label for="RSRV_ROOM_CNT">예약 객실 수 </label><input type="number" id="RSRV_ROOM_CNT" name="RSRV_ROOM_CNT" value="1"
                                                      min="1">
        <label for="OVNT_CNT">박수</label><input type="text" id="OVNT_CNT" name="OVNT_CNT" value="1" min="1">
        <label for="INHS_CUST_NM">투숙 고객명</label><input type="text" id="INHS_CUST_NM" name="INHS_CUST_NM" value="김지윤">
        <label for="INHS_CUST_TEL_NO2">투숙 고객 전화번호2</label><input type="text" id="INHS_CUST_TEL_NO2" name="INHS_CUST_TEL_NO2"
                                                          value="010">
        <label for="INHS_CUST_TEL_NO3">투숙 고객 전화번호3</label><input type="text" id="INHS_CUST_TEL_NO3" name="INHS_CUST_TEL_NO3"
                                                          value="0000">
        <label for="INHS_CUST_TEL_NO4">투숙 고객 전화번호4</label><input type="text" id="INHS_CUST_TEL_NO4" name="INHS_CUST_TEL_NO4"
                                                          value="0000">
        <label for="RSRV_CUST_NM">예약자 명</label><input type="text" id="RSRV_CUST_NM" name="RSRV_CUST_NM" value="김지윤">
        <label for="RSRV_CUST_TEL_NO2">예약자 전화 번호2</label><input type="text" id="RSRV_CUST_TEL_NO2" name="RSRV_CUST_TEL_NO2"
                                                          value="010">
        <label for="RSRV_CUST_TEL_NO3">예약자 전화 번호3</label><input type="text" id="RSRV_CUST_TEL_NO3" name="RSRV_CUST_TEL_NO3"
                                                          value="0000">
        <label for="RSRV_CUST_TEL_NO4">예약자 전화 번호4</label><input type="text" id="RSRV_CUST_TEL_NO4" name="RSRV_CUST_TEL_NO4"
                                                          value="0000">
        <label for="REFRESH_YN">리프레쉬 여부</label><input type="text" id="REFRESH_YN" name="REFRESH_YN" value="Y">
        <label for="RSRV_DATE">예약 일자</label><input type="text" id="RSRV_DATE" name="RSRV_DATE" value="20230706"
                                                  maxlength="8" minlength="8">
        <button>submit</button>
    </div>
</form>


</body>
</html>